package se.gu.ait.logserver;

/*
MatzJB 2015 Oct
Log server for pacemaker

Todo: fix Nullpointerexception (31/10) thread is not exiting correctly

http://www.rhcedan.com/2010/06/22/killing-a-java-thread/

Check that the headers can be read by Processing
*/

import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;
import java.text.*;
import se.gu.ait.matz_utils.*;


public class Pacemaker_server_logger
{

   private int port=8080;
   private String serverroot="./serverroot/";


   public void setServerSetting()
   {
      setServerSetting(port);
   }
    
   public void setServerSetting(int pn)
   {
      setServerSetting(pn,serverroot);
   }
    
   public void setServerSetting(int pn,String sr)
   {
      if (pn<=1024)
         System.out.println("The number on the port is too low, try another one!");
   }   


   public void go() throws Exception
   
   {
      ServerSocket listenSocket = new ServerSocket(port);
   
      System.out.println("Pacemaker logger by Matz JB.");
      System.out.println("Server is listening on port " + port + "...");
   
      while(true)
      {
      
         Socket connectionSocket = listenSocket.accept(); //blocks until established connection
      
         new Request_process(connectionSocket, port, serverroot);
      }
   }
   public static void main(String argv[]) throws Exception
   {
   
      int portNr=0;
      String path="";
         
      System.out.println("Starting server");
      Pacemaker_server_logger server = new Pacemaker_server_logger();
   
      server.setServerSetting(portNr,path);
   
      server.go();
   
   }//main

}//class Pacemaker_server_logger



class Request_process implements Runnable
{
   public Thread process = new Thread(this);
   private Socket so;
   private BufferedReader inFromClient;
   private DataOutputStream outToClient;

   private String serverroot 	= "";
   

   private int buffer_size     = 10;//bytes
   private String http_version = "HTTP/1.1";

   private String crlf 	= "\r\n";
   private String sp 		= " ";

   private DateFormat IETFformat;
   private Date now;



   public Request_process(Socket s, int port, String root) throws Exception
   {
      serverroot=root;
      so=s;//connection socket
   
      inFromClient = new BufferedReader(new InputStreamReader(so.getInputStream()));
      outToClient = new DataOutputStream(so.getOutputStream());
      process.start();
   }


   public String Pacemaker_log(String request)
   {
      
      String[] parts = Ut.split(request," ");
      String ret = addtolog(parts[1]); // second element of {"GET" "/456_434..."}
               
               
      return ret;
      //return "HTTP/1.1 200 OK\r\nDate: Thu, 29 Oct 2015 21:35:07 GMT\r\nContent-Length:0\r\nConnection: close\r\n\r\n";
   
   
   }
   
   //new code (29/10)
   public String addtolog( String entry)
   {
   
      PrintWriter out;
      int id;
      String userid;
      String ret ="HTTP/1.1 200 OK\r\nDate: Thu, 29 Oct 2015 21:35:07 GMT\r\nContent-Length:0\r\nConnection: close\r\n\r\n"; //return data
   
      //expects 
      String[] parts = Ut.split(entry, "_");
   
      if (parts.length!=3)
         System.out.println("syntax was wrong, found [" + entry+"]");
      
      userid = parts[0].substring(1); //remove /
      System.out.println("userid " + userid);
      
      try
      {
         id = Integer.parseInt(userid); //parse user id
      }
      catch (NumberFormatException e)
      {
         System.out.println("User ID was not an integer, skipping");
         return  "HTTP/1.1 400 Number Format Exception\r\nDate: Thu, 29 Oct 2015 21:35:07 GMT\r\nContent-Length:0\r\nConnection: close\r\n\r\n";
      }
   
      try
      {
      
         if (id>=0 && id<=5 ) //user id range
         {
            out = new PrintWriter(new BufferedWriter(new FileWriter("user_" + id + ".log", true/*append*/)));
         
            out.print(parts[1]);
            out.print(" " + parts[2]);
            out.println("");
            out.close();
            System.out.println("OK - Wrote event to disk");
            return ret;
         }
         else
            System.out.println("Userid not within accetable range [0,5]. Provided user ID:" + userid);
      }
      catch (IOException e) 
      {
         System.out.println("Unknown Event: Something went wrong writing to file.");
         return  "HTTP/1.1 400 IO Exception\r\nDate: Thu, 29 Oct 2015 21:35:07 GMT\r\nContent-Length:0\r\nConnection: close\r\n\r\n";
      }
      
   
   //unexpected error   
      return  "HTTP/1.1 418 I'm a teapot\r\nDate: Thu, 29 Oct 2015 21:35:07 GMT\r\nContent-Length:0\r\nConnection: close\r\n\r\n";
      
   }





//simply return with the answer
   public void run()
   {
      String temp_line="";
      String request_lines="";
      String tmp = "";
      
      try
      {
         while(!(temp_line = inFromClient.readLine()).equals("")) //l�s till dess blankrad p�tr�ffas
            request_lines+="|"+temp_line;
      
         tmp = Pacemaker_log(request_lines);
        
         outToClient.writeBytes(tmp);
      }
      
      catch(IOException r)//should not happen?
      {
         Ut.disperror("I/O failure","client better try again",90);
      
      }
   
         
   }
      
}//request_process

