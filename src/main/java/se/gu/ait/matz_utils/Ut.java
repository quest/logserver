package se.gu.ait.matz_utils;
/*
version 0.1 post crisis
*/

import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;
import java.text.*;


public class Ut
{
   public static void printf(String str)
   {
      printf(str, true);
   }		

   public static void printf(String str, boolean newline)
   {
      if (newline)
         System.out.println(str);
      else
      {
         System.out.print(str);
         System.out.flush();      
      }
   }



//�ndra till procent ist�llet!
/*
Denna funktionen �r lite mer sofistikerad, den anv�nder sig av exceptions och
olika niv�er av fel, 80-100% ger ett felmeddelande som manar anv�ndaren att
skicka bugrapport till skaparen

Id�n �r att en klass skall anv�nda disperror f�r att f� ett enhetligt s�tt
och informativt s�tt att f� reda p� var felet uppstod (stacktrace) och vad man kan g�ra f�r att
f�rhindra att det h�nder igen (prevention).
Man kan t�nka sig att implication-arrayen skall ut�kas till att omfatta
fler/f�rre niv�er av felmeddelande.

Man kan ytterligare t�nka sig att en klass kan tolerera ett visst antal fel
genom att l�ta en klassvariabel ackumuleras...

disperror med 2 argument �r en enklare variant f�r vanliga utskrifter
*/

   public static void disperror(String message, String where)
   {
      printf("\n\n:"+where+"/"+message+"\n");
   }

   public static void disperror(String message, String prevention, int level)//degree<-[0,5] 0 "REALISE" .. 5 "FATAL"
   {
      String[] implication = {
         "NOTE","OBSERVE","WARNING","ERROR","FATAL"};
      int index_lvl=(int) (level/100.)*implication.length;
   
      if (level>100 || level<0)
      {
         disperror("level of seriousness argument out of bounds","check argument<-[0,100]", 4);
         System.exit(0);
      }
   //printf(implication[seriousness]+":"+from+" /"+message+"==>"+prevention);
   
      if (level>80)//ERROR and FATAL yield this
      {
         printf("*****************************************************************************");
         printf("*     if you can see this -please- send a bugreport to Matz.johansson@gmail.com*");
         printf("*****************************************************************************\n");
      }
   
      try{}
      
      catch(IllegalArgumentException IAE)
      {
         IAE.printStackTrace();
      }
   
      throw new IllegalArgumentException("\n\n"+implication[index_lvl]+": /"+message+" --> "+prevention+"\n");
   }


/*
skapar korrekt meddelande till klienten innan en fil skickas, en fil m�ste alltid skickas
vare sig det �r en fil som man sj�lv vill ha eller ett felmeddelande.
run skall sedan skicka filen till klienten
*/


 //**********************************************************************
 //version 0.4

   public static String extract_suffix(String filename)
   
   {
   //go through the filename from last character
      int size = filename.length();
      int dot_index=0;
   
      for (int i=size-1; i>0; i--)//news! size-1
         if (filename.charAt(i)=='.')
         {
            dot_index=i;
            break;
         }
   
      String suffix="";
   
      for (int i=0;dot_index+i+1<size;i++)
         suffix+=filename.charAt(dot_index+i+1);
   
      return suffix;
   }


 //this function is used for filenames and for checking if wildcards are misused 
 //checks if the string contains the given character repeatedly

   public static boolean is_repeating(String filename, char character)
   
   {
      int index=-1;
   
      for (int i=0;i<filename.length();i++)
      {
         if (index>-1 && (filename.charAt(i)==character) && (i==index+1))
            return true;
      
         if (filename.charAt(i)==character)
            index=i;
      //save the index where the first character was spotted
      }
   
      return false;
   }


 //A function to view arrays of strings (could be seened in
 //Systemprogrammering i C and algorithms course)
 /*
  * converts the elements of an array of strings to a string with
  * tabs as separators (or whichever separator you want) @ param
  * truncate boolean value true if you want to truncate the
  * elements.  @ param truncate2size truncates the elements in x to
  * the be the size truncate2size or not truncated if element is
  * shorter than this value.
  */
 //perhaps separator would be avaiable in future version


   public static String array2String(String[] x)
   
   {
      return array2String(x, "{,}", 0, x.length-1);
   }

   public static String array2String(String[] x, String paren_comma)
   
   {
      return array2String(x, paren_comma, 0, x.length-1);
   }


   public static String array2String(String[] x, String paren_comma, int indexstart, int indexstop)
   
   {
      return array2String(x, paren_comma, indexstart, indexstop, false, -1);
   }


   public static String array2String(String[] x, String paren_comma, int index_start, int index_stop, boolean truncate, int truncate2size)
   
   {
      if (paren_comma.length()!=3)
      {
         System.out.println("indexstop too large");      
         System.exit(0);
      }
   		//disperror("paren_comma="+paren_comma+" is not in syntax <left bracket>+<comma/separator>+<right bracket>", "check paren_comma", 4);  
   
      String array = "";
      String separator = ""+paren_comma.charAt(1);//" ";//default "\t" make it look nicer
   
      if (index_stop > x.length)
      {
         System.out.println("indexstop too large");      
         System.exit(0);
      }
      int delta = index_start;
      int size = index_stop-index_start+1;
   
      for (int i=0; i<size; i++)
      {
         if (i%size != 0 && size != 0)
            array+=separator;
      
         if (truncate)
            array+=x[i+delta].substring(0, Math.min( truncate2size, (x[i+delta]).length() ) );
         else
            array+=x[i+delta];
      }
   
      return paren_comma.charAt(0)+array+paren_comma.charAt(2);
   }



//Goes through a battery of tests to check if the arguments are ok
 //for testing purpose

   public static void isparseOK(String message, String[] expected, String space) throws IllegalArgumentException
   {
      isparseOK(message, expected, space, "|");
   }


   public static void isparseOK(String message, String[] expected, String space,String separator) throws IllegalArgumentException
   
   {
   
      isparseOK(message, expected, space, separator, "*");
   }


   public static void isparseOK(String message, String[] expected, String space, String separator, String wildcard) throws IllegalArgumentException
   
   {
   
      if (space.length()==0)
         disperror("Space is empty, this is used as a separator in message, why do you want this behaviour?" , "space is preferably \" \"", 3);
   
      if (separator.length()==0)
      //invariant7
      
      //if separator is empty then the expected array should
      //contain only of wildcards else disp error
         for (int i=0;i<expected.length;i++)
            if (!expected[i].equals(wildcard))
               disperror("Separator is empty, this is used as a not-matching-token in the parser" , "make sure separator is consistent (preferably \"|\")", 3);
   
   
   //separator is a char in this version but its argument is a
   //string for nicer code
      if (separator.length()>1)
         disperror("Separator=\""+separator+"\" larger than 1" , "this ought to be a one character string", 3);
   
      if (wildcard.length()==0)
         disperror("Wildcard empty string" , "check wildcard, perferably \"*\"", 4);
   //SUSPECTS:if wildcard is the same as space???
   
   //see lemma1 in docum.
      boolean separator_ok=true;
      for (int i=0;i<message.length();i++)
         if ((""+message.charAt(i)).equals(separator))
            separator_ok=false;
   
      if (!separator_ok)//this sould occur if | used in message, its useless...
         disperror("Occurance of separator '"+separator+"' in message="+message, "don't let it be part of message=\""+message+"\"", 4);
   
   //get the words and compare them with the expected ones  
      StringTokenizer mess_tok = new StringTokenizer(message, space);
   
      String exp_string = array2String(expected);
   
   //consecutive separators in expected yield empty tokens
      if (separator.length()!=0)
      {
         if (is_repeating(exp_string, separator.charAt(0)))
         
            disperror("Consecutive separators found in expected=\""+array2String(expected)+"\", parseIT will not function with that syntax (see docum. for more info.)","check expected",4);
      }
   
      if(is_repeating(exp_string, wildcard.charAt(0)))
      
         disperror("Consecutive wildcards found in message=\""+message+"\", parseIT will not function with that syntax", "check message",4);
   
   
   
   //use split on the expected and sep do give an example how the syntax is
      int noft = mess_tok.countTokens();
   
      if (noft!=expected.length) 
         disperror("expected syntax \""+array2String(expected)+"\" differs from message=\""+message+"\"","check use of separators/pattern",3);
   //if noft==0 then ...
   
   
   /*
   If space is found in expected (the array of strings)
   ==> since message uses space to separate the tokens
   the tokens themselves will not contain space chars
   ergo to have space chars in expected is a waste of
   time, is also considered as an error in the usage of
   the function (this is used to signal that you may
   want to use other separators)
   
   */
   //invariant 1
      boolean space_ok=true;
      for (int i=0;i<noft;i++)
         for (int j=0;j<expected[i].length();j++)
            if ((""+expected[i].charAt(j))==space)
               space_ok=false;
   
      if (!space_ok)
         disperror("Occurance of char '"+separator+"' in 'expected' array","check use of separators/pattern", 3);
   }


 //this function is a sort of ambitious attempt to create a parser,
 //although it sufficient for this laboration, this parser is the foundation
 //for other courses since I always see myself try to get those
 //stringtokenizers work properly

   public static String[] parseIT(String message, String[] expected)
   
   {
      return parseIT(message, expected, " ");
   }


   public static String[] parseIT(String message, String[] expected, String space)
   
   {
      return parseIT(message, expected, space, "|");
   }


   public static String[] parseIT(String message, String[] expected, String space, String separator)
   
   
   {
      return parseIT(message, expected, space, separator, "*");
   }


 //senaste funderingar:om det finns fler tokens i message �n i expected, skall man returnera {|} d�?
 //det verkar som om tidskomplexiteten �r O(a*b) dvs. f�r det mesta kvadratisk,
 //a=length(message) b=max(length(expected[]))


   public static String[] parseIT(String message, String[] expected, String space, String separator, String wildcard) throws IllegalArgumentException
   
   {      
      StringTokenizer mess_tok = new StringTokenizer(message, space);
      String[] ans = new String[expected.length];// numbers of tokens      
   
   //id�: b�rja med att fylla en ans array med separatorer, n�r funktionen
   //�r klar s� ligger en dr�s med separatorer och visar
   //vilka delar som inte passade/som inte gicks igenom 
      for (int i=0; i<expected.length; i++)
         ans[i] = separator;
   
      StringTokenizer exp_tok;
   
   //goes thru the words and finds them in the expected see dokumentation.txt for more info
   
      for (int i=0; mess_tok.hasMoreTokens(); i++)
      {
      //not enough elements in expected --> wrong in expected or message
         try
         {
            exp_tok = new StringTokenizer(expected[i], separator);
         }
         
         catch(ArrayIndexOutOfBoundsException aioob)
            
         {
            return split(separator,"");
         }
         String mess_part = mess_tok.nextToken();
         String exp_part="";
      
         do
         {
            if (!exp_tok.hasMoreTokens())//if an error like this is displayed, recheck the tests!!!
            {
               printf("<VALIDITY ERROR> this should not happen! since tests executes the program upon error");    
               disperror("no tokens","separators must contain at least one option",4);
            }
            exp_part = exp_tok.nextToken();//first part
         
         //SUGGESTION:A future feature would be to let the patern match function do additional checking
         //of the tokens 
            if (exp_part.equals(wildcard))///nu ovanf�r endwith eftersom den matchar annat ocks�
            {
            //maybe use patternmatch??
               ans[i] = mess_part;
               break;//no need to look any further		   
            }
         
         //go through exp_part and mess_part which ever that is shorter
         //just added min
         
            if (exp_part.endsWith(wildcard))//special case "...*" * kan vara tomma str�ngen!
            {
               if (mess_part.length()+1<exp_part.length())// "/" "/*" �r ok
               {
                  ans[i]=separator;
                  break;
               }
            
               boolean match = true;
               for (int r=0; r<Math.min(exp_part.length()-1,mess_part.length()-1); r++)//check except last char ==> wildcard
                  if (mess_part.charAt(r) != exp_part.charAt(r))
                     match = false;
            
            
               if (match)
               {
                  ans[i] = mess_part;
                  break;
               }
            }
         
         //beta version
            if (exp_part.startsWith(wildcard))//special case "*..."
            {
            
               if (mess_part.length()+1<exp_part.length())// "/" "*/" �r ok
               {
                  ans[i]=separator;
                  break;
               }
               boolean match = true;
               for (int r=exp_part.length()+1; r>0; r--)//check except first char ==> wildcard
                  if (mess_part.charAt(r) != exp_part.charAt(r))
                     match = false;
            
               if (match)
               {
                  ans[i] = mess_part;
                  break;
               }
            }
         
         // matches the word in mess_tok, if the expected is wildcard then it matches everything
            if (exp_part.equals(mess_part)) //a match
            {
               ans[i] = mess_part;
               break;
            }			
            else
               ans[i] = separator; //miss �r separator eftersom den inte �r tvetydig (se invariant 2)
         }
         while(exp_tok.hasMoreTokens());
      }
   
      return ans;
   }//parseIT



 //****************************************************************

 //split::splits a string into an array of strings, using the
 //splitter as a guideline where to split could be programmed a
 //nicer way, but I think I should use parseIT (after all this work
 //:-/ )


   public static String[] split(String message, String splitter)
   
   {
      return split(message, splitter, "*");
   }


   public static String[] split(String message, String splitter, String wildcard)
   
   {
      StringTokenizer message_tok= new StringTokenizer(message, splitter);
      String[] expected = new String[message_tok.countTokens()];
   
   //expect everything, let everything through
      for(int q=0; q<expected.length; q++)
         expected[q]=wildcard;
   
   //SUGGESTION:what if splitter is | ==> check this if I use ""
   //then its illegal, if I use | then it could be illegal since
   //I usually use | in expected
      return parseIT(message, expected, splitter, "");//see lemmas in dokumentation
   }

}//class Mutil